app.directive('noSpecialChar', function() {
    
    "use strict";
    
    return {
        require: 'ngModel',
        link: function(scope, element, attrs, shoppingItemsController) {
            
            function priceInNumber(text) {
                if (text) {
                    var replacedInput = text.replace(/[^0-9]/g, '');
                    
                    //  -- /[^0-9]\s*[^0-9]*/g
                    
                    if (replacedInput !== text) {
                        
                        shoppingItemsController.$setViewValue(replacedInput);
                        shoppingItemsController.$render();
                    }
                    return replacedInput;
                }
                return undefined;
            }            
            shoppingItemsController.$parsers.push(priceInNumber);
        }
    }
});