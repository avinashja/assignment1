app.controller('shoppingItemsController',["$http", "$state", "$log", "$rootScope",
           "$location","$timeout","shareDataShopConfirmService",function($http, $state, $log, $rootScope, $location, $timeout, shareDataShopConfirmService){
    
                    
    "use strict";
                    
    var shopCtrl = this;
                    
    shopCtrl.disableCartIcon = true;
                 
    shopCtrl.showLoader = true;

    if(shareDataShopConfirmService.getCartItemsObj()){
        shopCtrl.cartData = shareDataShopConfirmService.getCartItemsObj();
        
        shopCtrl.cartDataCopy = [];
        angular.copy(shopCtrl.cartData, shopCtrl.cartDataCopy);
                
        $log.log(shopCtrl.cartDataCopy);
        
        shopCtrl.totalAmt = shareDataShopConfirmService.getTotalAmountObj();
        
        shopCtrl.firstVisit = false;
        $rootScope.itemCountInCart = shopCtrl.cartData.length;
        if(screen.width <= 500){
            shopCtrl.showHideCartOnClick = false;  
            shopCtrl.showOnAdd = false;
        }
        else{
            shopCtrl.showHideCartOnClick = true;   
            shopCtrl.showOnAdd = true;
        }
    }
    else{
        shopCtrl.cartData = [];
        shopCtrl.cartDataCopy = [];
        shopCtrl.totalAmt = 0;
        shopCtrl.showOnAdd = false;
        shopCtrl.firstVisit = true;
        $rootScope.itemCountInCart = 0;
        shopCtrl.showHideCartOnClick = false;
    }
    
    shopCtrl.getShoppingItems = function() {
        var baseUrl = "/data/shopping-items-data.json"; 
            
        $http.get(baseUrl).then(function mySucces(response) {
            shopCtrl.shoppingItemData = response.data;
            
            shopCtrl.showLoader = false;
            
            if(shopCtrl.firstVisit === false){
                for(var i=0; i<shopCtrl.cartData.length; i++){
                    for(var j=0;j<shopCtrl.shoppingItemData.shoppingItems.length; j++){
                        if(shopCtrl.cartData[i].id === shopCtrl.shoppingItemData.shoppingItems[j].id){
                            shopCtrl.shoppingItemData.shoppingItems[j].quantity = shopCtrl.cartData[i].quantity;
                        }        
                    }
                }
            }
            $log.log(shopCtrl.shoppingItemData);
            
        }, function myError(error) {
            $log.error('failure loading shopping data', error);
        });
    };
    
    shopCtrl.getShoppingItems();
    
    shopCtrl.addItemToCart = function(itemData){
    
        if(itemData.quantity === undefined){
            itemData.quantity = 1;
        }
        if (screen.width <= 519) {
            $log.log("mobile device");
            if(shopCtrl.showHideCartOnClick){
                shopCtrl.showOnAdd = true;    
            }
            else
                shopCtrl.showHideCartOnClick = false;
        }
        else{
            $log.log("other devices");
            shopCtrl.showOnAdd = true;
            shopCtrl.showHideCartOnClick = true;
        }
        
        $log.log(" check item data quamtity = "+itemData.quantity);
        var num = 0;
        
        if(shopCtrl.cartData != ""){
            for(var i=0; i<shopCtrl.cartDataCopy.length; i++){
                if(shopCtrl.cartDataCopy[i].id === itemData.id){
                    num = i;
                    if(shopCtrl.cartDataCopy[i].quantity === itemData.quantity)
                        return 0;
                    else{
                        shopCtrl.totalAmt = 
                            shopCtrl.totalAmt - (itemData.price * shopCtrl.cartDataCopy[i].quantity);
                        shopCtrl.cartDataCopy.splice(i, 1);
                    }
                }
            }
        }
    
        angular.copy(shopCtrl.cartDataCopy, shopCtrl.cartData);
        shopCtrl.cartData.push(itemData);
        angular.copy(shopCtrl.cartData, shopCtrl.cartDataCopy);
        
        $rootScope.itemCountInCart = shopCtrl.cartData.length;
        $log.log(shopCtrl.cartData);
        
        shopCtrl.totalAmt = shopCtrl.totalAmt + (itemData.price * itemData.quantity);
        console.log(shopCtrl.totalAmt);
    };
    
    shopCtrl.confirmOrder = function(target){
        
        shareDataShopConfirmService.addCartIemsObj(shopCtrl.cartData);
        shareDataShopConfirmService.addTotalAmountObj(shopCtrl.totalAmt);
        
        $rootScope.disableConfirmOrder = false;
        
        $state.go('confirmOrder');
    };
    
    shopCtrl.toggleCart = function(){
        if(shopCtrl.cartData.length === 0){
            shopCtrl.disableCartIcon = true;
        }
        else{
            shopCtrl.disableCartIcon = false;
            shopCtrl.showHideCartOnClick = !shopCtrl.showHideCartOnClick;
            shopCtrl.showOnAdd = !shopCtrl.showOnAdd;
        }
    };
}]);