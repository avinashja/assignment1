app.factory('httpInterceptor',['$q', '$rootScope','$log', function ($q, $rootScope, $log) {

    "use strict";
    
    return {
        request: function(config) {
            return config;
        },
        requestError: function(rejection) {
            $log.error('Request error:', rejection);
            return $q.reject(rejection);
        },
        response: function(response) {
            return response;
        },
        responseError: function(rejection) {
            $log.error('Response error:', rejection);
            return $q.reject(rejection);
        }
    }
    
}]);