app.service('shareDataShopConfirmService', [function(){
    
    "use strict";
    
    var cartItemsObj = "";

    this.addCartIemsObj = function(newObj) {
        cartItemsObj = newObj;
    };

    this.getCartItemsObj = function(){
        return cartItemsObj;
    };
    
    var totalAmountObj = "";
    
    this.addTotalAmountObj = function(newObj) {
        totalAmountObj = newObj;
    };

    this.getTotalAmountObj = function(){
        return totalAmountObj;
    };
    
    var customerDetailsObj = "";
    
    this.addCustomerDetailsObj = function(newObj){
        customerDetailsObj = newObj;
    }
    
    this.getCustomerDetailsObj = function(){
        return customerDetailsObj;
    }
    
}]);