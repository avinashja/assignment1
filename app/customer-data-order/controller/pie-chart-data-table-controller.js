app.controller('pieChartDataTableController',['$http', '$log', '$timeout', 'shareDataShopConfirmService', function($http, $log, $timeout, shareDataShopConfirmService){
    
    "use strict";
    
    var pieChartCtrl = this;
    
    var baseUrl = "/data/shopping-items-data.json"; 
            
    $http.get(baseUrl).then(function mySucces(response) {
        pieChartCtrl.shoppingItemData = response.data;

        $log.log(pieChartCtrl.shoppingItemData);

        pieChartCtrl.priceRangeCalcForChart(); 
        pieChartCtrl.cartItemsCalcForChart();

    }, function myError(error) {
        $log.error('failure loading shopping data', error);
    });
    
    pieChartCtrl.labels = [];
    pieChartCtrl.data = [];
    
    pieChartCtrl.labelsOfCart = [];
    pieChartCtrl.dataOfCart = [];
    
    pieChartCtrl.options = { legend: { display: true } };
    
    var priceRange200 = [],
        priceRange400 = [],
        priceRange600 = [],
        priceRange800 = [],
        priceRange1000 = [],
        priceRange1200 = [];
        
    pieChartCtrl.wholePriceRangeArray = [ priceRange200, priceRange400, priceRange600,                                        priceRange800, priceRange1000, priceRange1200];
        
    pieChartCtrl.priceRangeCalcForChart = function(){
    
        var lengthOfData = pieChartCtrl.shoppingItemData.shoppingItems.length;
        for(var j=0;j<lengthOfData; j++){
            
            var n = pieChartCtrl.shoppingItemData.shoppingItems[j].price / 100;
            
            n = Math.ceil(n);
            if(n % 2 != 0){
                n = n+1;
            }
            $log.log(n);
            
            pieChartCtrl.wholePriceRangeArray[n/2-1]
                .push(pieChartCtrl.shoppingItemData.shoppingItems[j]);
        }
        
        for(var i = 0; i < pieChartCtrl.wholePriceRangeArray.length; i++){
            if(pieChartCtrl.wholePriceRangeArray[i] != ""){
                pieChartCtrl.labels.push("price < "+ (200+(i*200)) + " - No of Items  ");
                
                var noOfItems = pieChartCtrl.wholePriceRangeArray[i].length;
                pieChartCtrl.data.push(noOfItems);
            }    
            
        }
        $log.log(pieChartCtrl.data);
        $log.log(pieChartCtrl.labels);
     };
    
    pieChartCtrl.cartItemsCalcForChart = function(){
    
        var cartData = shareDataShopConfirmService.getCartItemsObj();
        for(var i = 0; i < cartData.length; i++) {
            pieChartCtrl.labelsOfCart.push(cartData[i].id+" : quantity ");
            pieChartCtrl.dataOfCart.push(cartData[i].quantity);
        }
    };
    
    pieChartCtrl.optionsDataTable = {
        rowHeight: 70,
        headerHeight: 50,
        footerHeight: false,
        columnMode: 'force',
        columns: [
            {
                name: "Id",
                prop: "id",
                width: 200
            }, 
            {
                name: "Item",
                prop: "path",
                cellRenderer: function(prop) {
                    return '<div><img width="60" height="60" src="'+prop.$cell+'"></div>';
                }
            }, 
            {
                name: "Name",
                prop: "name"
            }, 
            {
                name: "Price",
                prop: "price"
            },
            {
                name: "Quantity",
                prop: "quantity"
            }
        ]
    };
    
    pieChartCtrl.selected = [];
    
    pieChartCtrl.onClick = function(points, evt){
        pieChartCtrl.dataOfDataTable = [];
        
        $log.log(points, evt);
        $log.log(points[0]._model.label.split(" ")[1]);
        if(points[0]._model.label.split(" ")[1] == ':'){
            
            var currentItemId = points[0]._model.label.split(" ")[0];
            var cartData = shareDataShopConfirmService.getCartItemsObj();
            var currentItemData = {};
            for(var i=0; i< cartData.length; i++){
                if(cartData[i].id == currentItemId){
                    
                    currentItemData = cartData[i];
                    pieChartCtrl.dataOfDataTable.push(currentItemData); 
                }
            }
        }
        else{
            var priceRangeForItems = points[0]._model.label.split(" ")[2];
            var num = priceRangeForItems/200;
            pieChartCtrl.dataOfDataTable = pieChartCtrl.wholePriceRangeArray[num-1];
        }
    };
}]);