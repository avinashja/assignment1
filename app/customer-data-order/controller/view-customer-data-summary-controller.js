app.controller('viewCustomerDataSummaryController',["$state", "$log", "$rootScope", "shareDataShopConfirmService",function($state, $log, $rootScope, shareDataShopConfirmService){
    
    "use strict";
    
    var customerCtrl = this;
    customerCtrl.userData = shareDataShopConfirmService.getCustomerDetailsObj();
    
    $log.log(customerCtrl.userData);
    
    customerCtrl.orderItems = function(){
        $rootScope.disableOrderItem = false;
        $state.go('orderItem');
    };
}]);