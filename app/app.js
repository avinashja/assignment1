var app = angular.module("shopping-cart-app", ['ui.router', 'data-table', 'chart.js']);


app.config(function($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider) {

    "use strict";
    
    $locationProvider.html5Mode(true);
    
    $httpProvider.interceptors.push("httpInterceptor");
    
    $urlRouterProvider.otherwise('/home');
    
    $stateProvider
        
        .state('home', {
            url: '/home',
            templateUrl: 'shopping-items/shopping-items.html'
        })
    
        .state('confirmOrder', {
            url: '/confirmOrder',
            templateUrl: 'confirm-items/confirm-items.html'
        })
    
        .state('customerDataOrder',{
            url: '/customerDataOrder',
            templateUrl: 'customer-data-order/customer-data-order.html'
        })
        
        .state('orderItem',{
            url: '/orderItem',
            templateUrl: 'payment-option/payment-option.html'
        })
});