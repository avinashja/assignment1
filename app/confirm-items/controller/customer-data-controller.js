app.controller('customerDataController',["$state", "$rootScope", "shareDataShopConfirmService",function($state, $rootScope, shareDataShopConfirmService){
    
    "use strict";
    
    var custDataCtrl = this;
    
    if(shareDataShopConfirmService.getCustomerDetailsObj){
        
        var userData = shareDataShopConfirmService.getCustomerDetailsObj();
        
        custDataCtrl.user = {};
        
        custDataCtrl.user.username = userData.username;
        custDataCtrl.user.address = userData.address;
        custDataCtrl.user.pincode = userData.pincode;
        custDataCtrl.user.mobile = userData.mobile;
        custDataCtrl.user.email = userData.email;
    }
    else{
        custDataCtrl.user = {};
    }
    
    custDataCtrl.toOrderSummary = function(userData){
        
        shareDataShopConfirmService.addCustomerDetailsObj(userData);
        $rootScope.disableOrderSummary = false;
        $state.go('customerDataOrder');
    };
    
    custDataCtrl.reset = function(customerForm) {
        
        custDataCtrl.user = {};
        if (customerForm) {
            customerForm.$setPristine();
            customerForm.$setUntouched();
        }
    };
}]);