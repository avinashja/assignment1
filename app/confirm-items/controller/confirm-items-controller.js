app.controller('confirmItemsController',["$scope", "$state", "$http", "$log", "shareDataShopConfirmService",function($scope, $state, $http, $log, shareDataShopConfirmService){
    
    "use strict";
    
    var itemsCtrl = this;
    
    itemsCtrl.itemsToBeOrdered = shareDataShopConfirmService.getCartItemsObj();
    
    $log.log(itemsCtrl.itemsToBeOrdered);
    
    itemsCtrl.totalAmount = shareDataShopConfirmService.getTotalAmountObj();
    
    $log.log(itemsCtrl.totalAmount);
    
}]);