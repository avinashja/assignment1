var app = angular.module("shopping-cart-app", ['ui.router', 'data-table', 'chart.js']);


app.config(function($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider) {

    "use strict";
    
    $locationProvider.html5Mode(true);
    
    $httpProvider.interceptors.push("httpInterceptor");
    
    $urlRouterProvider.otherwise('/home');
    
    $stateProvider
        
        .state('home', {
            url: '/home',
            templateUrl: 'shopping-items/shopping-items.html'
        })
    
        .state('confirmOrder', {
            url: '/confirmOrder',
            templateUrl: 'confirm-items/confirm-items.html'
        })
    
        .state('customerDataOrder',{
            url: '/customerDataOrder',
            templateUrl: 'customer-data-order/customer-data-order.html'
        })
        
        .state('orderItem',{
            url: '/orderItem',
            templateUrl: 'payment-option/payment-option.html'
        })
});
app.factory('httpInterceptor',['$q', '$rootScope','$log', function ($q, $rootScope, $log) {

    "use strict";
    
    return {
        request: function(config) {
            return config;
        },
        requestError: function(rejection) {
            $log.error('Request error:', rejection);
            return $q.reject(rejection);
        },
        response: function(response) {
            return response;
        },
        responseError: function(rejection) {
            $log.error('Response error:', rejection);
            return $q.reject(rejection);
        }
    }
    
}]);
app.service('shareDataShopConfirmService', [function(){
    
    "use strict";
    
    var cartItemsObj = "";

    this.addCartIemsObj = function(newObj) {
        cartItemsObj = newObj;
    };

    this.getCartItemsObj = function(){
        return cartItemsObj;
    };
    
    var totalAmountObj = "";
    
    this.addTotalAmountObj = function(newObj) {
        totalAmountObj = newObj;
    };

    this.getTotalAmountObj = function(){
        return totalAmountObj;
    };
    
    var customerDetailsObj = "";
    
    this.addCustomerDetailsObj = function(newObj){
        customerDetailsObj = newObj;
    }
    
    this.getCustomerDetailsObj = function(){
        return customerDetailsObj;
    }
    
}]);
app.directive("confirmItemsDirective", function(){
    
    "use strict";
    
    return{
       templateUrl: "confirm-items/confirm-items-directive-template.html"
    } 
}); 
app.directive("customerDataDirective", function(){
    
    "use strict";
    
    return{
       templateUrl: "confirm-items/customer-data-directive-template.html"
    } 
}); 
app.controller('confirmItemsController',["$scope", "$state", "$http", "$log", "shareDataShopConfirmService",function($scope, $state, $http, $log, shareDataShopConfirmService){
    
    "use strict";
    
    var itemsCtrl = this;
    
    itemsCtrl.itemsToBeOrdered = shareDataShopConfirmService.getCartItemsObj();
    
    $log.log(itemsCtrl.itemsToBeOrdered);
    
    itemsCtrl.totalAmount = shareDataShopConfirmService.getTotalAmountObj();
    
    $log.log(itemsCtrl.totalAmount);
    
}]);
app.controller('customerDataController',["$state", "$rootScope", "shareDataShopConfirmService",function($state, $rootScope, shareDataShopConfirmService){
    
    "use strict";
    
    var custDataCtrl = this;
    
    if(shareDataShopConfirmService.getCustomerDetailsObj){
        
        var userData = shareDataShopConfirmService.getCustomerDetailsObj();
        
        custDataCtrl.user = {};
        
        custDataCtrl.user.username = userData.username;
        custDataCtrl.user.address = userData.address;
        custDataCtrl.user.pincode = userData.pincode;
        custDataCtrl.user.mobile = userData.mobile;
        custDataCtrl.user.email = userData.email;
    }
    else{
        custDataCtrl.user = {};
    }
    
    custDataCtrl.toOrderSummary = function(userData){
        
        shareDataShopConfirmService.addCustomerDetailsObj(userData);
        $rootScope.disableOrderSummary = false;
        $state.go('customerDataOrder');
    };
    
    custDataCtrl.reset = function(customerForm) {
        
        custDataCtrl.user = {};
        if (customerForm) {
            customerForm.$setPristine();
            customerForm.$setUntouched();
        }
    };
}]);
app.controller('pieChartDataTableController',['$http', '$log', '$timeout', 'shareDataShopConfirmService', function($http, $log, $timeout, shareDataShopConfirmService){
    
    "use strict";
    
    var pieChartCtrl = this;
    
    var baseUrl = "/data/shopping-items-data.json"; 
            
    $http.get(baseUrl).then(function mySucces(response) {
        pieChartCtrl.shoppingItemData = response.data;

        $log.log(pieChartCtrl.shoppingItemData);

        pieChartCtrl.priceRangeCalcForChart(); 
        pieChartCtrl.cartItemsCalcForChart();

    }, function myError(error) {
        $log.error('failure loading shopping data', error);
    });
    
    pieChartCtrl.labels = [];
    pieChartCtrl.data = [];
    
    pieChartCtrl.labelsOfCart = [];
    pieChartCtrl.dataOfCart = [];
    
    pieChartCtrl.options = { legend: { display: true } };
    
    var priceRange200 = [],
        priceRange400 = [],
        priceRange600 = [],
        priceRange800 = [],
        priceRange1000 = [],
        priceRange1200 = [];
        
    pieChartCtrl.wholePriceRangeArray = [ priceRange200, priceRange400, priceRange600,                                        priceRange800, priceRange1000, priceRange1200];
        
    pieChartCtrl.priceRangeCalcForChart = function(){
    
        var lengthOfData = pieChartCtrl.shoppingItemData.shoppingItems.length;
        for(var j=0;j<lengthOfData; j++){
            
            var n = pieChartCtrl.shoppingItemData.shoppingItems[j].price / 100;
            
            n = Math.ceil(n);
            if(n % 2 != 0){
                n = n+1;
            }
            $log.log(n);
            
            pieChartCtrl.wholePriceRangeArray[n/2-1]
                .push(pieChartCtrl.shoppingItemData.shoppingItems[j]);
        }
        
        for(var i = 0; i < pieChartCtrl.wholePriceRangeArray.length; i++){
            if(pieChartCtrl.wholePriceRangeArray[i] != ""){
                pieChartCtrl.labels.push("price < "+ (200+(i*200)) + " - No of Items  ");
                
                var noOfItems = pieChartCtrl.wholePriceRangeArray[i].length;
                pieChartCtrl.data.push(noOfItems);
            }    
            
        }
        $log.log(pieChartCtrl.data);
        $log.log(pieChartCtrl.labels);
     };
    
    pieChartCtrl.cartItemsCalcForChart = function(){
    
        var cartData = shareDataShopConfirmService.getCartItemsObj();
        for(var i = 0; i < cartData.length; i++) {
            pieChartCtrl.labelsOfCart.push(cartData[i].id+" : quantity ");
            pieChartCtrl.dataOfCart.push(cartData[i].quantity);
        }
    };
    
    pieChartCtrl.optionsDataTable = {
        rowHeight: 70,
        headerHeight: 50,
        footerHeight: false,
        columnMode: 'force',
        columns: [
            {
                name: "Id",
                prop: "id",
                width: 200
            }, 
            {
                name: "Item",
                prop: "path",
                cellRenderer: function(prop) {
                    return '<div><img width="60" height="60" src="'+prop.$cell+'"></div>';
                }
            }, 
            {
                name: "Name",
                prop: "name"
            }, 
            {
                name: "Price",
                prop: "price"
            },
            {
                name: "Quantity",
                prop: "quantity"
            }
        ]
    };
    
    pieChartCtrl.selected = [];
    
    pieChartCtrl.onClick = function(points, evt){
        pieChartCtrl.dataOfDataTable = [];
        
        $log.log(points, evt);
        $log.log(points[0]._model.label.split(" ")[1]);
        if(points[0]._model.label.split(" ")[1] == ':'){
            
            var currentItemId = points[0]._model.label.split(" ")[0];
            var cartData = shareDataShopConfirmService.getCartItemsObj();
            var currentItemData = {};
            for(var i=0; i< cartData.length; i++){
                if(cartData[i].id == currentItemId){
                    
                    currentItemData = cartData[i];
                    pieChartCtrl.dataOfDataTable.push(currentItemData); 
                }
            }
        }
        else{
            var priceRangeForItems = points[0]._model.label.split(" ")[2];
            var num = priceRangeForItems/200;
            pieChartCtrl.dataOfDataTable = pieChartCtrl.wholePriceRangeArray[num-1];
        }
    };
}]);
app.controller('viewCustomerDataSummaryController',["$state", "$log", "$rootScope", "shareDataShopConfirmService",function($state, $log, $rootScope, shareDataShopConfirmService){
    
    "use strict";
    
    var customerCtrl = this;
    customerCtrl.userData = shareDataShopConfirmService.getCustomerDetailsObj();
    
    $log.log(customerCtrl.userData);
    
    customerCtrl.orderItems = function(){
        $rootScope.disableOrderItem = false;
        $state.go('orderItem');
    };
}]);
app.directive("pieChartDataTableDirective", function(){
    
    "use strict";
    
    return{
       templateUrl: "customer-data-order/pie-chart-data-table-directive-template.html"
    } 
}); 
app.directive("viewCustomerDataDirective", function(){
    
    "use strict";
    
    return{
       templateUrl: "customer-data-order/view-customer-data-directive-template.html"
    } 
}); 
app.directive('noSpecialChar', function() {
    
    "use strict";
    
    return {
        require: 'ngModel',
        link: function(scope, element, attrs, shoppingItemsController) {
            
            function priceInNumber(text) {
                if (text) {
                    var replacedInput = text.replace(/[^0-9]/g, '');
                    
                    //  -- /[^0-9]\s*[^0-9]*/g
                    
                    if (replacedInput !== text) {
                        
                        shoppingItemsController.$setViewValue(replacedInput);
                        shoppingItemsController.$render();
                    }
                    return replacedInput;
                }
                return undefined;
            }            
            shoppingItemsController.$parsers.push(priceInNumber);
        }
    }
});
app.directive("shoppingItemsDirective", function(){
    
    "use strict";
    
    return{
       templateUrl: "shopping-items/shopping-items-directive-template.html"
    } 
}); 
app.controller('shoppingItemsController',["$http", "$state", "$log", "$rootScope",
           "$location","$timeout","shareDataShopConfirmService",function($http, $state, $log, $rootScope, $location, $timeout, shareDataShopConfirmService){
    
                    
    "use strict";
                    
    var shopCtrl = this;
                    
    shopCtrl.disableCartIcon = true;
                 
    shopCtrl.showLoader = true;

    if(shareDataShopConfirmService.getCartItemsObj()){
        shopCtrl.cartData = shareDataShopConfirmService.getCartItemsObj();
        
        shopCtrl.cartDataCopy = [];
        angular.copy(shopCtrl.cartData, shopCtrl.cartDataCopy);
                
        $log.log(shopCtrl.cartDataCopy);
        
        shopCtrl.totalAmt = shareDataShopConfirmService.getTotalAmountObj();
        
        shopCtrl.firstVisit = false;
        $rootScope.itemCountInCart = shopCtrl.cartData.length;
        if(screen.width <= 500){
            shopCtrl.showHideCartOnClick = false;  
            shopCtrl.showOnAdd = false;
        }
        else{
            shopCtrl.showHideCartOnClick = true;   
            shopCtrl.showOnAdd = true;
        }
    }
    else{
        shopCtrl.cartData = [];
        shopCtrl.cartDataCopy = [];
        shopCtrl.totalAmt = 0;
        shopCtrl.showOnAdd = false;
        shopCtrl.firstVisit = true;
        $rootScope.itemCountInCart = 0;
        shopCtrl.showHideCartOnClick = false;
    }
    
    shopCtrl.getShoppingItems = function() {
        var baseUrl = "/data/shopping-items-data.json"; 
            
        $http.get(baseUrl).then(function mySucces(response) {
            shopCtrl.shoppingItemData = response.data;
            
            shopCtrl.showLoader = false;
            
            if(shopCtrl.firstVisit === false){
                for(var i=0; i<shopCtrl.cartData.length; i++){
                    for(var j=0;j<shopCtrl.shoppingItemData.shoppingItems.length; j++){
                        if(shopCtrl.cartData[i].id === shopCtrl.shoppingItemData.shoppingItems[j].id){
                            shopCtrl.shoppingItemData.shoppingItems[j].quantity = shopCtrl.cartData[i].quantity;
                        }        
                    }
                }
            }
            $log.log(shopCtrl.shoppingItemData);
            
        }, function myError(error) {
            $log.error('failure loading shopping data', error);
        });
    };
    
    shopCtrl.getShoppingItems();
    
    shopCtrl.addItemToCart = function(itemData){
    
        if(itemData.quantity === undefined){
            itemData.quantity = 1;
        }
        if (screen.width <= 519) {
            $log.log("mobile device");
            if(shopCtrl.showHideCartOnClick){
                shopCtrl.showOnAdd = true;    
            }
            else
                shopCtrl.showHideCartOnClick = false;
        }
        else{
            $log.log("other devices");
            shopCtrl.showOnAdd = true;
            shopCtrl.showHideCartOnClick = true;
        }
        
        $log.log(" check item data quamtity = "+itemData.quantity);
        var num = 0;
        
        if(shopCtrl.cartData != ""){
            for(var i=0; i<shopCtrl.cartDataCopy.length; i++){
                if(shopCtrl.cartDataCopy[i].id === itemData.id){
                    num = i;
                    if(shopCtrl.cartDataCopy[i].quantity === itemData.quantity)
                        return 0;
                    else{
                        shopCtrl.totalAmt = 
                            shopCtrl.totalAmt - (itemData.price * shopCtrl.cartDataCopy[i].quantity);
                        shopCtrl.cartDataCopy.splice(i, 1);
                    }
                }
            }
        }
    
        angular.copy(shopCtrl.cartDataCopy, shopCtrl.cartData);
        shopCtrl.cartData.push(itemData);
        angular.copy(shopCtrl.cartData, shopCtrl.cartDataCopy);
        
        $rootScope.itemCountInCart = shopCtrl.cartData.length;
        $log.log(shopCtrl.cartData);
        
        shopCtrl.totalAmt = shopCtrl.totalAmt + (itemData.price * itemData.quantity);
        console.log(shopCtrl.totalAmt);
    };
    
    shopCtrl.confirmOrder = function(target){
        
        shareDataShopConfirmService.addCartIemsObj(shopCtrl.cartData);
        shareDataShopConfirmService.addTotalAmountObj(shopCtrl.totalAmt);
        
        $rootScope.disableConfirmOrder = false;
        
        $state.go('confirmOrder');
    };
    
    shopCtrl.toggleCart = function(){
        if(shopCtrl.cartData.length === 0){
            shopCtrl.disableCartIcon = true;
        }
        else{
            shopCtrl.disableCartIcon = false;
            shopCtrl.showHideCartOnClick = !shopCtrl.showHideCartOnClick;
            shopCtrl.showOnAdd = !shopCtrl.showOnAdd;
        }
    };
}]);