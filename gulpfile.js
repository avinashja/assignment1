'use strict';

var gulp = require('gulp');
var concatCss = require('gulp-concat-css');
var concat = require('gulp-concat');  
var rename = require('gulp-rename');  
var uglify = require('gulp-uglify');  
var sass = require('gulp-sass');

//script paths
var jsFiles = 'app/**/*.js',  
    jsDest = 'build/';

var sassFiles = 'app/assets/sass/*.sass';
	
gulp.task('default',['scripts', 'sass', 'watch']);


gulp.task('scripts', function() {
    console.log("scripts task started running");
    return gulp.src(jsFiles)
        .pipe(concat('scripts.js'))
        .pipe(gulp.dest(jsDest))
        .pipe(rename('scripts.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest(jsDest));
});


gulp.task("sass", function(){
	console.log("Generate CSS files " + (new Date()).toString());
    return gulp.src(sassFiles)
        .pipe(sass({ style: 'expanded' }))
        .pipe(concat('shopping-cart.css'))
		.pipe(gulp.dest(jsDest));
});


gulp.task("watch", function(){
	console.log("Watching sass/js files for modifications");
    gulp.watch(jsFiles, ["scripts"]);
    gulp.watch(sassFiles, ["sass"]);
    
});
