var express = require("express");
var app     = express();

var appDir = "/app";

app.use("/", express.static(__dirname + appDir));

console.log(__dirname);
console.log(__dirname + appDir);
console.log(appDir);

app.use('/angular/js', express.static(__dirname + '/node_modules/angular'));
app.use('/angular-ui-router/js', express.static(__dirname + '/node_modules/angular-ui-router/release'));
app.use('/jquery/js', express.static(__dirname + '/node_modules/jquery/dist'));
app.use('/bootstrap/js', express.static(__dirname + '/node_modules/bootstrap/dist/js'));
app.use('/bootstrap/css', express.static(__dirname + '/node_modules/bootstrap/dist/css'));
app.use('/bootstrap/fonts', express.static(__dirname + '/node_modules/bootstrap/fonts'));
app.use('/angular-data-table', express.static(__dirname + '/node_modules/angular-data-table/release'));
app.use('/angular-chart.js/js', express.static(__dirname + '/node_modules/angular-chart.js/dist'));
app.use('/chart.js/js', express.static(__dirname + '/node_modules/chart.js/dist'));

app.use("/build", express.static(__dirname + "/build"));

app.get('/',function(req,res){
    res.sendFile('./app/index.html', {"root": __dirname});
    //It will find and locate index.html from View or Scripts
});

app.listen(3000);

console.log("Running at Port 3000");